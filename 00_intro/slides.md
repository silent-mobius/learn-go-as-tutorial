# Go Programming



.footer: Created By Alex M. Schapelle, VAioLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is GoLang programming ?
- Who needs it ?
- Basic concepts of GoLang programming.
- Looping and Branching.
- Structures, Maps, Functions and Libraries.


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of GoLang Programming
- For junior/senior developers who are still developing on LAMP/LEMP/WAMP/MAMP stacks and do automation.
- Experienced ops who need refresher


---

# Course Topics

- Intro
- Basics Go programming
- Operators within GoLang
- Storing data with arrays and slices
- Conditions
- Loops
- Functions
- Structs
- Maps

---
# About Me
<img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- over 12 years of IT industry Experience.
- fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - between each semester, I tried to take IT course at various places.
        - yes, one of them was A+.
        - yes, one of them was Cisco.
        - yes, one of them was RedHat course.
        - yes, one of them was LPIC1 and Shell scripting.
        - no, others i learned alone.
        - no, not maintaining debian packages any more.
---

# About Me (cont.)
- over 7 years of sysadmin:
    - shell scripting fanatic
    - python developer
    - js admirer
    - golang fallen
    - rust fan
- 5 years of working with devops
    - git supporter
    - vagrant enthusiast
    - ansible consultant
    - container believer
    - k8s user

you can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# What is Go?

    Go is a cross-platform, open source programming language
    Go can be used to create high-performance applications
    Go is a fast, statically typed, compiled language that feels like a dynamically typed, interpreted language
    Go was developed at Google by Robert Griesemer, Rob Pike, and Ken Thompson in 2007
    Go's syntax is similar to C++
---

# What is Go Used For?

    Web development (server-side)
    Developing network-based programs
    Developing cross-platform enterprise applications
    Cloud-native development
---
# Why Use Go?

    Go is fun and easy to learn
    Go has fast run time and compilation time
    Go supports concurrency
    Go has memory management
    Go works on different platforms (Windows, Mac, Linux, Raspberry Pi, etc.)

---
# Why Use Go? (cont.)

Go Compared to Python and C++

| Go |	Python | 	C++ |
| -- |  ---    |  ---   |
| Statically typed |	Dynamically typed |   Statically typed |
| Fast run time    |    Slow run time 	  |   Fast run time    |
| Compiled         |    Interpreted 	  |   Compiled         |
| Fast compile time  |	Interpreted       |   Slow compile time |
| Supports concurrency through goroutines and channel | No built-in concurrency mechanism | Supports concurrency through threads |
| Has automatic garbage collection | Has automatic garbage collection | Does not have automatic garbage collection |
| Does not support classes and objects | 	Has classes and objects   | Has classes and objects |
| Does not support inheritance      | Supports inheritance 	          | Supports inheritance |

> Notes:

    Compilation time refers to translating the code into an executable program
    Concurrency is performing multiple things out-of-order, or at the same time, without affecting the final outcome
    Statically typed means that the variable types are known at compile time



---
# History


---

# Setup

 - Go Install

You can find the relevant installation files at https://golang.org/dl/.

Follow the instructions related to your operating system. To check if Go was installed successfully, you can run the following command in a terminal window:
go version

Which should show the version of your Go installation.
---

# Setup (cont.)


 - Go Install IDE

An IDE (Integrated Development Environment) is used to edit AND compile the code.

Popular IDE's include Visual Studio Code (VS Code), Vim, Eclipse, and Notepad. These are all free, and they can be used to both edit and debug Go code.

> Note: Web-based IDE's can work as well, but functionality is limited.

We will use VS Code in our tutorial, which we believe is a good place to start.

You can find the latest version of VS Code at https://code.visualstudio.com/.
---
# Setup (cont.)

- Go Quickstart
- Let's create our first Go program.
  - Launch the VS Code editor
  - Open the extension manager or alternatively, press Ctrl + Shift + x
  - In the search box, type "go" and hit enter
  - Find the Go extension by the GO team at Google and install the extension
  - After the installation is complete, open the command palette by pressing Ctrl + Shift + p
  - Run the Go: Install/Update Tools command
  - Select all the provided tools and click OK

---

# Setup (cont.)

VS Code is now configured to use Go.

Open up a terminal window and type:
go mod init example.com/hello

Do not worry if you do not understand why we type the above command. Just think of it as something that you always do, and that you will learn more about in a later chapter.

Create a new file (File > New File). Copy and paste the following code and save the file as helloworld.go (File > Save As):
helloworld.go

```go
package main
import ("fmt")

func main() {
  fmt.Println("Hello World!")
}
```

---
# Setup (cont.)

Now, run the code: Open a terminal in VS Code and type:

```sh
go run ./helloworld.go
Hello World!
```
Congratulations! You have now written and executed your first Go program.

If you want to save the program as an executable, type and run:
```sh
go build ./helloworld.go 
```