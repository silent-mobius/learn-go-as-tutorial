# Learn GO As Tutorial

This is initial course material for learning Go Programming language as step by step tutorial

- Pre Req
  - English language is mandatory
  - Existing knowledge in programming/scripting language.
  - Somewhat of, understanding programming concepts.
  - Knowledge in current/latest editor/IDE environments.