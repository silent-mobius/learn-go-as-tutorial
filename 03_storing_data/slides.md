# Go Programming



.footer: Created By Alex M. Schapelle, VAioLabs.io

---


# Go Arrays

Arrays are used to store multiple values of the same type in a single variable, instead of declaring separate variables for each value.
Declare an Array

In Go, there are two ways to declare an array:
1. With the var keyword:
Syntax
```go
var array_name = [length]datatype{values} // here length is defined
```
or
```go
var array_name = [...]datatype{values} // here length is inferred
```
---
2. With the := sign:
Syntax
```go
array_name := [length]datatype{values} // here length is defined
```
or
```go
array_name := [...]datatype{values} // here length is inferred
```
> Note: The length specifies the number of elements to store in the array. In Go, arrays have a fixed length. The length of the array is either defined by a number or is inferred (means that the compiler decides the length of the array, based on the number of values).

---

# Array Examples

This example declares two arrays (arr1 and arr2) with defined lengths:
```go
package main
import ("fmt")

func main() {
  var arr1 = [3]int{1,2,3}
  arr2 := [5]int{4,5,6,7,8}

  fmt.Println(arr1)
  fmt.Println(arr2)
}
```
Result:
```sh
[1 2 3]
[4 5 6 7 8]
```

This example declares two arrays (arr1 and arr2) with inferred lengths:
```go
package main
import ("fmt")

func main() {
  var arr1 = [...]int{1,2,3}
  arr2 := [...]int{4,5,6,7,8}

  fmt.Println(arr1)
  fmt.Println(arr2)
}
```
Result:
```sh
[1 2 3]
[4 5 6 7 8]
```
---

This example declares an array of strings:

```go
package main
import ("fmt")

func main() {
  var cars = [4]string{"Volvo", "BMW", "Ford", "Mazda"}
  fmt.Print(cars)
}
```
Result:

```sh
[Volvo BMW Ford Mazda]
```
---

# Access Elements of an Array

You can access a specific array element by referring to the index number.
In Go, array indexes start at 0. That means that [0] is the first element, [1] is the second element, etc.
This example shows how to access the first and third elements in the prices array:
```go
package main
import ("fmt")

func main() {
  prices := [3]int{10,20,30}

  fmt.Println(prices[0])
  fmt.Println(prices[2])
}
```
Result:
```sh
10
30
```
---

# Change Elements of an Array

You can also change the value of a specific array element by referring to the index number.
This example shows how to change the value of the third element in the prices array: 

```go
package main
import ("fmt")

func main() {
  prices := [3]int{10,20,30}

  prices[2] = 50
  fmt.Println(prices)
}
```


Result:
```sh
[10 20 50]
```
---
# Array Initialization

If an array or one of its elements has not been initialized in the code, it is assigned the default value of its type.

> Tip: The default value for int is 0, and the default value for string is "".
```go
package main
import ("fmt")

func main() {
  arr1 := [5]int{} //not initialized
  arr2 := [5]int{1,2} //partially initialized
  arr3 := [5]int{1,2,3,4,5} //fully initialized

  fmt.Println(arr1)
  fmt.Println(arr2)
  fmt.Println(arr3)
}
```
Result:
```go
[0 0 0 0 0]
[1 2 0 0 0]
[1 2 3 4 5]
```
---
# Initialize Only Specific Elements

It is possible to initialize only specific elements in an array.

This example initializes only the second and third elements of the array: 
```go
package main
import ("fmt")

func main() {
  arr1 := [5]int{1:10,2:40}

  fmt.Println(arr1)
}
```

Result:
```sh
[0 10 40 0 0]
```
---
# Example Explained

The array above has 5 elements.
```sh
1:10 means: assign 10 to array index 1 (second element).
2:40 means: assign 40 to array index 2 (third element).
```
---
# Find the Length of an Array

The len() function is used to find the length of an array:
```go
package main
import ("fmt")

func main() {
  arr1 := [4]string{"Volvo", "BMW", "Ford", "Mazda"}
  arr2 := [...]int{1,2,3,4,5,6}

  fmt.Println(len(arr1))
  fmt.Println(len(arr2))
}
```
Result:
```sh
4
6 
```
---
# Go Slices

Slices are similar to arrays, but are more powerful and flexible.
Like arrays, slices are also used to store multiple values of the same type in a single variable.
However, unlike arrays, the length of a slice can grow and shrink as you see fit.
In Go, there are several ways to create a slice:
- Using the []datatype{values} format
- Create a slice from an array
- Using the make() function

Create a Slice With []datatype{values}
Syntax
```go
slice_name := []datatype{values}
```
A common way of declaring a slice is like this:
```go
myslice := []int
```
The code above declares an empty slice of 0 length and 0 capacity.

To initialize the slice during declaration, use this:
```go
myslice := []int{1,2,3}
```
The code above declares a slice of integers of length 3 and also the capacity of 3.

---

In Go, there are two functions that can be used to return the length and capacity of a slice:

- len() function - returns the length of the slice (the number of elements in the slice)
- cap() function - returns the capacity of the slice (the number of elements the slice can grow or shrink to)

This example shows how to create slices using the []datatype{values} format:
```go
package main
import ("fmt")

func main() {
  myslice1 := []int{}
  fmt.Println(len(myslice1))
  fmt.Println(cap(myslice1))
  fmt.Println(myslice1)

  myslice2 := []string{"Go", "Slices", "Are", "Powerful"}
  fmt.Println(len(myslice2))
  fmt.Println(cap(myslice2))
  fmt.Println(myslice2)
}
```
Result:
```sh
0
0
[]
4
4
[Go Slices Are Powerful]
```

In the example above, we see that in the first slice (myslice1), the actual elements are not specified, so both the length and capacity of the slice will be zero. In the second slice (myslice2), the elements are specified, and both length and capacity is equal to the number of actual elements specified.

--- 

# Create a Slice From an Array

You can create a slice by slicing an array:
Syntax
```go
var myarray = [length]datatype{values} // An array
myslice := myarray[start:end] // A slice made from the array
```

This example shows how to create a slice from an array:
```go
package main
import ("fmt")

func main() {
  arr1 := [6]int{10, 11, 12, 13, 14,15}
  myslice := arr1[2:4]

  fmt.Printf("myslice = %v\n", myslice)
  fmt.Printf("length = %d\n", len(myslice))
  fmt.Printf("capacity = %d\n", cap(myslice))
}
```
Result:
```sh
myslice = [12 13]
length = 2
capacity = 4
```

In the example above myslice is a slice with length 2. It is made from arr1 which is an array with length 6.
The slice starts from the second element of the array which has value 12. The slice can grow to the end of the array. This means that the capacity of the slice is 4.
If myslice started from element 0, the slice capacity would be 6.
---

# Create a Slice With The make() Function

The make() function can also be used to create a slice.
Syntax
`slice_name := make([]type, length, capacity)`

> Note: If the capacity parameter is not defined, it will be equal to length.

This example shows how to create slices using the make() function:
```go
package main
import ("fmt")

func main() {
  myslice1 := make([]int, 5, 10)
  fmt.Printf("myslice1 = %v\n", myslice1)
  fmt.Printf("length = %d\n", len(myslice1))
  fmt.Printf("capacity = %d\n", cap(myslice1))

  // with omitted capacity
  myslice2 := make([]int, 5)
  fmt.Printf("myslice2 = %v\n", myslice2)
  fmt.Printf("length = %d\n", len(myslice2))
  fmt.Printf("capacity = %d\n", cap(myslice2))
}
```
Result:
```sh
myslice1 = [0 0 0 0 0]
length = 5
capacity = 10
myslice2 = [0 0 0 0 0]
length = 5
capacity = 5
```
---
# Access Elements of a Slice

You can access a specific slice element by referring to the index number.
In Go, indexes start at 0. That means that [0] is the first element, [1] is the second element, etc.
This example shows how to access the first and third elements in the prices slice:
```go
package main
import ("fmt")

func main() {
  prices := []int{10,20,30}

  fmt.Println(prices[0])
  fmt.Println(prices[2])
}
```
Result:
```sh
10
30
```

---
#Change Elements of a Slice

You can also change a specific slice element by referring to the index number.
This example shows how to change the third element in the prices slice:
```go
package main
import ("fmt")

func main() {
  prices := []int{10,20,30}
  prices[2] = 50
  fmt.Println(prices[0])
  fmt.Println(prices[2])
}
```
Result:
```sh
10
50
```
---

# Append Elements To a Slice

You can append elements to the end of a slice using the append()function:
Syntax
`slice_name = append(slice_name, element1, element2, ...)`
This example shows how to append elements to the end of a slice:
```go
package main
import ("fmt")

func main() {
  myslice1 := []int{1, 2, 3, 4, 5, 6}
  fmt.Printf("myslice1 = %v\n", myslice1)
  fmt.Printf("length = %d\n", len(myslice1))
  fmt.Printf("capacity = %d\n", cap(myslice1))

  myslice1 = append(myslice1, 20, 21)
  fmt.Printf("myslice1 = %v\n", myslice1)
  fmt.Printf("length = %d\n", len(myslice1))
  fmt.Printf("capacity = %d\n", cap(myslice1))
}
```
Result:
```sh
myslice1 = [1 2 3 4 5 6]
length = 6
capacity = 6
myslice1 = [1 2 3 4 5 6 20 21]
length = 8
capacity = 12
```
---
# Append One Slice To Another Slice

To append all the elements of one slice to another slice, use the append()function:
Syntax
slice3 = append(slice1, slice2...)

Note: The '...' after slice2 is necessary when appending the elements of one slice to another.
This example shows how to append one slice to another slice:
```go
package main
import ("fmt")

func main() {
  myslice1 := []int{1,2,3}
  myslice2 := []int{4,5,6}
  myslice3 := append(myslice1, myslice2...)

  fmt.Printf("myslice3=%v\n", myslice3)
  fmt.Printf("length=%d\n", len(myslice3))
  fmt.Printf("capacity=%d\n", cap(myslice3))
}
```
Result:
```sh
myslice3=[1 2 3 4 5 6]
length=6
capacity=6
```
---

# Change The Length of a Slice

Unlike arrays, it is possible to change the length of a slice.
This example shows how to change the length of a slice:
```go
package main
import ("fmt")

func main() {
  arr1 := [6]int{9, 10, 11, 12, 13, 14} // An array
  myslice1 := arr1[1:5] // Slice array
  fmt.Printf("myslice1 = %v\n", myslice1)
  fmt.Printf("length = %d\n", len(myslice1))
  fmt.Printf("capacity = %d\n", cap(myslice1))

  myslice1 = arr1[1:3] // Change length by re-slicing the array
  fmt.Printf("myslice1 = %v\n", myslice1)
  fmt.Printf("length = %d\n", len(myslice1))
  fmt.Printf("capacity = %d\n", cap(myslice1))

  myslice1 = append(myslice1, 20, 21, 22, 23) // Change length by appending items
  fmt.Printf("myslice1 = %v\n", myslice1)
  fmt.Printf("length = %d\n", len(myslice1))
  fmt.Printf("capacity = %d\n", cap(myslice1))
}
```
Result:
```sh
myslice1 = [10 11 12 13]
length = 4
capacity = 5
myslice1 = [10 11]
length = 2
capacity = 5
myslice1 = [10 11 20 21 22 23]
length = 6
capacity = 10
```
---

# Memory Efficiency

When using slices, Go loads all the underlying elements into the memory.
If the array is large and you need only a few elements, it is better to copy those elements using the copy() function.
The copy() function creates a new underlying array with only the required elements for the slice. This will reduce the memory used for the program. 
Syntax: `copy(dest, src)`
The `copy()` function takes in two slices dest and src, and copies data from src to dest. It returns the number of elements copied.

This example shows how to use the `copy()` function:
```go
package main
import ("fmt")

func main() {
  numbers := []int{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}
  // Original slice
  fmt.Printf("numbers = %v\n", numbers)
  fmt.Printf("length = %d\n", len(numbers))
  fmt.Printf("capacity = %d\n", cap(numbers))

  // Create copy with only needed numbers
  neededNumbers := numbers[:len(numbers)-10]
  numbersCopy := make([]int, len(neededNumbers))
  copy(numbersCopy, neededNumbers)

  fmt.Printf("numbersCopy = %v\n", numbersCopy)
  fmt.Printf("length = %d\n", len(numbersCopy))
  fmt.Printf("capacity = %d\n", cap(numbersCopy))
}
```
Result:
```sh
// Original slice
numbers = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
length = 15
capacity = 15
// New slice
numbersCopy = [1 2 3 4 5]
length = 5
capacity = 5
```
The capacity of the new slice is now less than the capacity of the original slice because the new underlying array is smaller.

