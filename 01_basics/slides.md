# Go Programming



.footer: Created By Alex M. Schapelle, VAioLabs.io

---
# Go Syntax

A Go file consists of the following parts:

    Package declaration
    Import packages
    Functions
    Statements and expressions

Look at the following code, to understand it better:

```go
package main
import ("fmt")

func main() {
  fmt.Println("Hello World!")
}
```
---

# Go Syntax

- Example explained
  - Line 1: In Go, every program is part of a package. We define this using the package keyword. In this example, the program belongs to the main package.
  - Line 2: import ("fmt") lets us import files included in the fmt package.
  - Line 3: A blank line. Go ignores white space. Having white spaces in code makes it more readable.
  - Line 4: func main() {} is a function. Any code inside its curly brackets {} will be executed.
  - Line 5: fmt.Println() is a function made available from the fmt package. It is used to output/print text. In our example it will output "Hello World!".

> Note: In Go, any executable code belongs to the main package.

---

# Go Statements
```go
fmt.Println("Hello World!") is a statement.
```
In Go, statements are separated by ending a line (hitting the Enter key) or by a semicolon ";".
Hitting the Enter key adds ";" to the end of the line implicitly (does not show up in the source code).
The left curly bracket { cannot come at the start of a line.

Run the following code and see what happens:

```go
package main
import ("fmt")

func main()
{
  fmt.Println("Hello World!")
} 
```
---

# Go Comments

A comment is a text that is ignored upon execution.
Comments can be used to explain the code, and to make it more readable.
Comments can also be used to prevent code execution when testing an alternative code.
Go supports single-line or multi-line comments.
Go Single-line Comments
Single-line comments start with two forward slashes (//).
Any text between // and the end of the line is ignored by the compiler (will not be executed).

---
# Go Comments

Example
```go
// This is a comment
package main
import ("fmt")

func main() {
  // This is a comment
  fmt.Println("Hello World!")
}
```
The following example uses a single-line comment at the end of a code line:
Example

```go
package main
import ("fmt")

func main() {
  fmt.Println("Hello World!") // This is a comment
}
```
---

# Go Multi-line Comments

Multi-line comments start with /* and ends with */.
Any text between /* and */ will be ignored by the compiler:
Example
```go
package main
import ("fmt")

func main() {
  /* The code below will print Hello World
  to the screen, and it is amazing */
  fmt.Println("Hello World!")
}
```
---
# Comment to Prevent Code Execution

You can also use comments to prevent the code from being executed.
The commented code can be saved for later reference and troubleshooting.
Example
```go
package main
import ("fmt")

func main() {
  fmt.Println("Hello World!")
  // fmt.Println("This line does not execute")
}
```
# Go variables

Variables are containers for storing data values.

## Go Variable Types

In Go, there are different types of variables, for example:
- int: stores integers (whole numbers), such as 123 or -123
- float32: stores floating point numbers, with decimals, such as 19.99 or -19.99
- string: stores text, such as "Hello World". String values are surrounded by double quotes
- bool: stores values with two states: true or false

More about different variable types, will be explained in the Go Data Types chapter.
Declaring (Creating) Variables

In Go, there are two ways to declare a variable:
1. With the `var` keyword:

Use the var keyword, followed by variable name and type:
Syntax
```go
var variablename type = value
```
> Note: You always have to specify either type or value (or both).
2. With the `:=` (walrus) sign:

Use the := sign, followed by the variable value:
Syntax
```go
variablename := value
```
> Note: In this case, the type of the variable is inferred from the value (means that the compiler decides the type of the variable, based on the value).

> Note: It is not possible to declare a variable using :=, without assigning a value to it.

---
# Variable Declaration With Initial Value

If the value of a variable is known from the start, you can declare the variable and assign a value to it on one line:
Example
```go
package main
import ("fmt")

func main() {
  var student1 string = "John" //type is string
  var student2 = "Jane" //type is inferred
  x := 2 //type is inferred

  fmt.Println(student1)
  fmt.Println(student2)
  fmt.Println(x)
}
```
> Note: The variable types of student2 and x is inferred from their values.
---

# Variable Declaration Without Initial Value

In Go, all variables are initialized. So, if you declare a variable without an initial value, its value will be set to the default value of its type:
Example
```go
package main
import ("fmt")

func main() {
  var a string
  var b int
  var c bool

  fmt.Println(a)
  fmt.Println(b)
  fmt.Println(c)
}
```

---
# Variable Declaration Without Initial Value (cont.)

Example explained

In this example there are 3 variables:
- a
- b
- c

These variables are declared but they have not been assigned initial values.

By running the code, we can see that they already have the default values of their respective types:
- a is ""
- b is 0
- c is false

---

# Value Assignment After Declaration

It is possible to assign a value to a variable after it is declared. This is helpful for cases the value is not initially known.
Example
```go
package main
import ("fmt")

func main() {
  var student1 string
  student1 = "John"
  fmt.Println(student1)
}
```

> Note: It is not possible to declare a variable using ":=" without assigning a value to it.

---

# Difference Between var and :=

- There are some small differences between the var var :=:

| var  | 	:= | 
| ---  |   --- |
| Can be used inside and outside of functions | 	Can only be used inside functions |
| Variable declaration and value assignment can be done separately |	Variable declaration and value assignment cannot be done separately (must be done in the same line)| 

---

Example

This example shows declaring variables outside of a function, with the var keyword:
```go
package main
import ("fmt")

var a int
var b int = 2
var c = 3

func main() {
  a = 1
  fmt.Println(a)
  fmt.Println(b)
  fmt.Println(c)
}
```
Example
---

Since `:=` is used outside of a function, running the program results in an error.
```go
package main
import ("fmt")

a := 1

func main() {
  fmt.Println(a)
}
```

Result:
```sh
./prog.go:5:1: syntax error: non-declaration statement outside function body 
```
---

# Go Constants

If a variable should have a fixed value that cannot be changed, you can use the const keyword.

The const keyword declares the variable as "constant", which means that it is unchangeable and read-only.
Syntax

```go
const CONSTNAME type = value
```
> Note: The value of a constant must be assigned when you declare it.
---

# Declaring a Constant

Here is an example of declaring a constant in Go:
Example
```go
package main
import ("fmt")

const PI = 3.14

func main() {
  fmt.Println(PI)
}
```
---

# Constant Rules

- Constant names follow the same naming rules as variables
- Constant names are usually written in uppercase letters (for easy identification and differentiation from variables)
- Constants can be declared both inside and outside of a function

---
# Constant Types

There are two types of constants:

- Typed constants
- Untyped constants

---

# Typed Constants

Typed constants are declared with a defined type:
Example
```go
package main
import ("fmt")

const A int = 1

func main() {
  fmt.Println(A)
}
```
---
# Untyped Constants

Untyped constants are declared without a type:
Example
```go
package main
import ("fmt")

const A = 1

func main() {
  fmt.Println(A)
}
```
> Note: In this case, the type of the constant is inferred from the value (means the compiler decides the type of the constant, based on the value).
---
# Constants: Unchangeable and Read-only

When a constant is declared, it is not possible to change the value later:
Example

```go
package main
import ("fmt")

func main() {
  const A = 1
  A = 2
  fmt.Println(A)
}
```
Result:
```sh
./prog.go:8:7: cannot assign to A
```
# Multiple Constants Declaration

Multiple constants can be grouped together into a block for readability:
Example
```go
package main
import ("fmt")

const (
  A int = 1
  B = 3.14
  C = "Hi!"
)

func main() {
  fmt.Println(A)
  fmt.Println(B)
  fmt.Println(C)
}
```
---

# Go Output Functions

Go has three functions to output text:

- `Print()`
- `Println()`
- `Printf()`

---
# The Print() Function

The Print() function prints its arguments with their default format.
Example

Print the values of i and j:
```go
package main
import ("fmt")

func main() {
  var i,j string = "Hello","World"

  fmt.Print(i)
  fmt.Print(j)
}
```
Result:
```sh
HelloWorld
```
---
#
If we want to print the arguments in new lines, we need to use \n.
```go
package main
import ("fmt")

func main() {
  var i,j string = "Hello","World"

  fmt.Print(i, "\n")
  fmt.Print(j, "\n")
}
```
Result:

```sh
Hello
World
```
> Tip: \n creates new lines.

Example

It is also possible to only use one Print() for printing multiple variables.
```go
package main
import ("fmt")

func main() {
  var i,j string = "Hello","World"

  fmt.Print(i, "\n",j)
}
```
Result:
```sh
Hello
World
```

Example

If we want to add a space between string arguments, we need to use " ":
```go
package main
import ("fmt")

func main() {
  var i,j string = "Hello","World"

  fmt.Print(i, " ", j)
}
```
Result:
```sh
Hello World
```
---
Example

Print() inserts a space between the arguments if neither are strings:
```go
package main
import ("fmt")

func main() {
  var i,j = 10,20

  fmt.Print(i,j)
}
```
Result:
```sh
10 20
```
----

# The Println() Function

The Println() function is similar to Print() with the difference that a whitespace is added between the arguments, and a newline is added at the end:
Example
```go
package main
import ("fmt")

func main() {
  var i,j string = "Hello","World"

  fmt.Println(i,j)
}
```
Result:
```sh
Hello World
```
---

# The Printf() Function

The Printf() function first formats its argument based on the given formatting verb and then prints them.

Here we will use two formatting verbs:

- %v is used to print the value of the arguments
- %T is used to print the type of the arguments

Example
```go
package main
import ("fmt")

func main() {
  var i string = "Hello"
  var j int = 15

  fmt.Printf("i has value: %v and type: %T\n", i, i)
  fmt.Printf("j has value: %v and type: %T", j, j)
}
```
Result:
```sh
i has value: Hello and type: string
j has value: 15 and type: int
```
---

# Go Data Types

Data type is an important concept in programming. Data type specifies the size and type of variable values in RAM.

Go is statically typed, meaning that once a variable type is defined, it can only store data of that type.

Go has three basic data types:

- bool: represents a boolean value and is either true or false
- Numeric: represents integer types, floating point values, and complex types
- string: represents a string value

Example

This example shows some of the different data types in Go:
```go
package main
import ("fmt")

func main() {
  var a bool = true     // Boolean
  var b int = 5         // Integer
  var c float32 = 3.14  // Floating point number
  var d string = "Hi!"  // String

  fmt.Println("Boolean: ", a)
  fmt.Println("Integer: ", b)
  fmt.Println("Float:   ", c)
  fmt.Println("String:  ", d)
}
```
---
# Boolean Data Type

A boolean data type is declared with the bool keyword and can only take the values true or false.

The default value of a boolean data type is false.
Example

This example shows some different ways to declare Boolean variables:

```go
package main
import ("fmt")

func main() {
  var b1 bool = true // typed declaration with initial value
  var b2 = true // untyped declaration with initial value
  var b3 bool // typed declaration without initial value
  b4 := true // untyped declaration with initial value

  fmt.Println(b1) // Returns true
  fmt.Println(b2) // Returns true
  fmt.Println(b3) // Returns false
  fmt.Println(bo4) // Returns true
}
```
> Note: Boolean values are mostly used for conditional testing which you will learn more about in the Go Conditions chapter.

---
# Go Integer Data Types

Integer data types are used to store a whole number without decimals, like 35, -50, or 1345000.

The integer data type has two categories:

- Signed integers - can store both positive and negative values
- Unsigned integers - can only store non-negative values

Tip: The default type for integer is int. If you do not specify a type, the type will be int.
Signed Integers

Signed integers, declared with one of the int keywords, can store both positive and negative values:
Example
```go
package main
import ("fmt")

func main() {
  var x int = 500
  var y int = -4500
  fmt.Printf("Type: %T, value: %v", x, x)
  fmt.Printf("Type: %T, value: %v", y, y)
}
```
---

# Go has five keywords/types of signed integers:
| Type | 	Size 	| Range |
| ---  |   ---      | ---   |
|int   | Depends on platform:         |     |
|       | 32 bits in 32 bit systems and|     |
|       |  64 bit in 64 bit systems 	  |-2147483648 to 2147483647 in 32 bit systems and  -9223372036854775808 to 9223372036854775807 in 64 bit systems|
|int8 	|8 bits/1 byte  |	-128 to 127 |
|int16 	|16 bits/2 byte |	-32768 to 32767 |
|int32 	|32 bits/4 byte |	-2147483648 to 2147483647 |
|int64 	|64 bits/8 byte |	-9223372036854775808 to 9223372036854775807 |
---

# Unsigned Integers

Unsigned integers, declared with one of the uint keywords, can only store non-negative values:
Example
```go
package main
import ("fmt")

func main() {
  var x uint = 500
  var y uint = 4500
  fmt.Printf("Type: %T, value: %v", x, x)
  fmt.Printf("Type: %T, value: %v", y, y)
}
```
---
# Go has five keywords/types of unsigned integers:


|Type | 	Size  | 	Range |
| --- |    ---    |    ---    |
|uint |	Depends on platform: | |
|       |32 bits in 32 bit systems and | |
|       |64 bit in 64 bit systems 	| 0 to 4294967295 in 32 bit systems and 0 to 18446744073709551615 in 64 bit systems |
|uint8 	| 8 bits/1 byte | 	0 to 255|
|uint16 |	16 bits/2 byte | 	0 to 65535 |
|uint32 | 32 bits/4 byte |	0 to 4294967295 |
|uint64 | 64 bits/8 byte |	0 to 18446744073709551615 |

---
# Which Integer Type to Use?

The type of integer to choose, depends on the value the variable has to store.
Example

This example will result in an error because 1000 is out of range for int8 (which is from -128 to 127):
```go
package main
import ("fmt")

func main() {
  var x int8 = 1000
  fmt.Printf("Type: %T, value: %v", x, x)
}
```
Result:

```sh
./prog.go:5:7: constant 1000 overflows int8 
```

---
# Go Float Data Types

The float data types are used to store positive and negative numbers with a decimal point, like 35.3, -2.34, or 3597.34987.

The float data type has two keywords:

|Type |	Size |	Range|
| --- | ---  |  ---  |
|float32 | 32 bits |	-3.4e+38 to 3.4e+38.  |
|float64 | 64 bits |	-1.7e+308 to +1.7e+308. |

> Tip: The default type for float is float64. If you do not specify a type, the type will be float64.
---

# The float32 Keyword
Example

This example shows how to declare some variables of type float32:
```go
package main
import ("fmt")

func main() {
  var x float32 = 123.78
  var y float32 = 3.4e+38
  fmt.Printf("Type: %T, value: %v\n", x, x)
  fmt.Printf("Type: %T, value: %v", y, y)
}
```
---
# The float64 Keyword

The float64 data type can store a larger set of numbers than float32.
Example

This example shows how to declare a variable of type float64:
```go
package main
import ("fmt")

func main() {
  var x float64 = 1.7e+308
  fmt.Printf("Type: %T, value: %v", x, x)
}

```
---
# Which Float Type to Use?

The type of float to choose, depends on the value the variable has to store.
Example

This example will result in an error because 3.4e+39 is out of range for float32:
```go
package main
import ("fmt")

func main() {
  var x float32= 3.4e+39
  fmt.Println(x)
}
```
Result:
```sh
./prog.go:5:7: constant 3.4e+39 overflows float32 
```
--- 
# String Data Type

The string data type is used to store a sequence of characters (text). String values must be surrounded by double quotes:
Example
```go
package main
import ("fmt")

func main() {
  var txt1 string = "Hello!"
  var txt2 string
  txt3 := "World 1"

  fmt.Printf("Type: %T, value: %\n", txt1, txt1)
  fmt.Printf("Type: %T, value: %v\n", txt2, txt2)
  fmt.Printf("Type: %T, value: %v\n", txt3, txt3)
}
```

Result:
```sh
Type: string, value: Hello!
Type: string, value:
Type: string, value: World 1
```